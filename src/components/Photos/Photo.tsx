import React from 'react';
import { IPhoto } from '../../models'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

interface PhotosProps {
    photo: IPhoto
}

function Photo(props: PhotosProps) {
    return (
        <Card sx={{ maxWidth: 345 }}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    height="140"
                    image={`${props.photo.url}`}
                    alt="color"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {props.photo.title}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}

export default Photo;