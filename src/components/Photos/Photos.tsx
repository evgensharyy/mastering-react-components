import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Photo from './Photo'
import { IPhoto } from '../../models'

function Photos(props: any) {
    const [photos, setPhotos] = useState<IPhoto[]>([])

    async function fetchPhotos() {
        const response = await axios.get<IPhoto[]>('https://jsonplaceholder.typicode.com/photos?_start=0&_limit=10')
        setPhotos(response.data)
    }

    useEffect(() => {
        fetchPhotos()
    }, [])

    return (
        <div>
            {props.children}
            {photos.map(photo => <Photo photo={photo} key={photo.id} />)}
        </div>
    );
}

export default Photos;