import React from 'react';
import { Title } from '../models';

interface FooterProps {
    title: Title,
}

function Footer(props: FooterProps) {
    return (
        <h2>{props.title || 'Footer title'}</h2>
    );
}

export default Footer;