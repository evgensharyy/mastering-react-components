import React from 'react';
import { Title } from '../models'

interface HeaderProps {
    title: Title,
}

function Header(props: HeaderProps) {
    return (
        <h1>{props.title || 'Header title'}</h1>
    );
}

export default Header;