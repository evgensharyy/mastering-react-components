import React from 'react';
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';
import './App.css';

function Page() {
  return (
    <>
    <Header title={'Header title'}/>
    <Main />
    <Footer title={'Footer title'} />
    </>
  );
}

export default Page;
